<?php

namespace App\Http\Resources\SoilCriteria;

use Illuminate\Http\Resources\Json\JsonResource;

class SoilCriteria extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'photo' => $this->photo ? asset('storage/' . $this->photo) : ''
        ]);
    }
}
