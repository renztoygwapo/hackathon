<?php

namespace App\Http\Resources\SoilCriteria;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SoilCriteriaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
