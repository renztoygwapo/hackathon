<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnionBank;
use App\User;

class UnionBankController extends Controller
{
    public function callback(Request $request)
    {
        if ($request->has('code')) {
            $unionBank = new UnionBank;
            $response = $unionBank->getAccessToken($request->get('code'));
            
            return view('unionbank', [
                'callbak' => $response
            ]);
            
            return response()->json('Successfully connected please return to your mobile app.');
        }
    }

    public function connectPayment()
    {
        $unionBank = new UnionBank;
        return redirect($unionBank->createPaymentURL());
    }

    public function connectAccountBalance()
    {
        $unionBank = new UnionBank;
        return redirect($unionBank->createAccountBalanceURL());
    }

    public function payment()
    {
        return view('unionbank');
    }
}
