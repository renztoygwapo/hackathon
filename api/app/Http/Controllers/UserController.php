<?php

namespace App\Http\Controllers;

use App\User;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = User::latest();
        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 10;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'username' => 'required|string',
            'mobile_no' => 'required|string',
            'address_city' => 'required|string',
            'address_province' => 'required|string',
            'address_brgy' => 'required|string',
            'address_code' => 'required|string',
            'user_type' => 'required|string'
        ]);

        $user = User::query()
            ->create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'username' => $request->username,
                'mobile_no' => $request->mobile_no,
                'address_city' => $request->address_city,
                'address_province' => $request->address_province,
                'address_brgy' => $request->address_brgy,
                'address_code' => $request->address_code,
                'user_type' => $request->user_type
            ]);

            return response()->json($user,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $item = User::where('id',$id)->get();
            return response()->json($item,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'username' => 'required|string',
            'mobile_no' => 'required|string',
            'address_city' => 'required|string',
            'address_province' => 'required|string',
            'address_brgy' => 'required|string',
            'address_code' => 'required|string',
            'user_type' => 'required|string'
        ]);

        try {
            $item = User::findOrFail($id);
            $item->username = $request->username;
            $item->first_name = $request->first_name;
            $item->last_name = $request->last_name;
            $item->middle_name = $request->middle_name;
            $item->address_city = $request->address_city;
            $item->address_province = $request->address_province;
            $item->address_brgy = $request->address_brgy;
            $item->address_code = $request->address_code;
            $item->user_type = $request->user_type;
            $item->mobile_no = $request->mobile_no;
            $item->save();
            
            return response()->json($item,200); 
        } catch (\Exception $exception) {
        throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = User::findOrFail($id);
            $item->delete();
            return response()->json('Success',200);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function getUserByUsername(Request $request)
    {
        try {
            $item = User::where('username', $request->username)->get();
            return response()->json($item,200);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
