<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SoilCriteria;
use App\Http\Resources\SoilCriteria\SoilCriteriaCollection;
use App\Http\Resources\SoilCriteria\SoilCriteria as SoilCriteriaResource;

class SoilCriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $soil_criteria = SoilCriteria::paginate($request->get('paginate', 10));
        return new SoilCriteriaCollection($soil_criteria);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required|string',
            'photo' => 'required|image',
            'area' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'color' => 'nullable|string'
        ]);

        $soil_criteria = SoilCriteria::create($request->except('photo'));
        
        if ($request->hasFile('photo')) {
            $soil_criteria->savePhoto($request->file('photo'));
        }

        return new SoilCriteriaResource($soil_criteria);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SoilCriteria $soil_criteria)
    {
        return new SoilCiteriaResource($soil_criteria);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => 'required|string',
            'photo' => 'required|image',
            'area' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'color' => 'nullable|string'
        ]);
        
        $soil_criteria = SoilCriteria::findOrFail($id);
        $soil_criteria->fill($request->except('photo'));
        $soil_criteria->save();

        if ($request->hasFile('photo')) {
            $soil_criteria->savePhoto($request->file('photo'));
        }
        
        return new SoilCriteriaResource($soil_criteria);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoilCriteria $soil_criteria)
    {
        $soil_criteria->delete();
    }
}
