<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\PurchaseItem;
use Illuminate\Http\Request;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\Product as ProductResource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::latest()->get();
        return new ProductCollection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $request->validate([
                'name' => 'required|string',
                'photo' => 'required|image',
                'price' => 'required|numeric|min:1',
                'category_id' => 'required|exists:categories,id',
                'description' => 'nullable|string',
                'status' => 'required|in:' . implode(',', Product::getStatuses())
            ]);
    
            $product = Product::create($request->except('photo'));
            
            if ($request->hasFile('photo')) {
                $product->savePhoto($request->file('photo'));
            }

            return new ProductResource($product);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'photo' => 'image',
            'price' => 'required|numeric|min:1',
            'category_id' => 'required|exists:categories,id',
            'description' => 'nullable|string',
            'status' => 'required|in:' . implode(',', Product::getStatuses())
        ]);
        $product = Product::find($id);
        $product->fill($request->except('photo'));
        $product->save();

        if ($request->hasFile('photo')) {
            $product->savePhoto($request->file('photo'));
        }
        
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    }

    public function productUser()
    {
        $products = Product::with(['user','category'])->get();
        return response()->json($products,200);
    }

    public function getProductsByCategoryName(Request $request)
    {
        $products = Category::where('name', $request->category_name)->with('products')->get();
        return response()->json($products,200);
    }

    public function countPurchasedProductsAndCropCreated()
    {
        $purchased_products = PurchaseItem::all();
        $categories = Category::all();
        $products = Product::all();
        $total_counts = [];

        foreach ($products as $product) {
            foreach ($categories as $category) {
                $total_counts[$category->name] = $category->products()->where('category_id', $product->category_id)->count();
            }
            $total_counts['purchased_products'] = count($purchased_products);
        }

        return response()->json($total_counts,200);
    }
}
