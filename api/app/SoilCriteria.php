<?php

namespace App;

use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class SoilCriteria extends Model
{
    protected $fillable = ['photo','area','question','color'];
    
    public function savePhoto(UploadedFile $file)
    {
        $filename = str_random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/areas/' . $this->id, $file, $filename);
        $this->photo = 'areas/' . $this->id . '/' . $filename;
        $this->save();
    }
}
