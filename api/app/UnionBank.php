<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnionBank extends Model
{
    public function getAccessToken($code)
    {
        $client = new \GuzzleHttp\Client();
        $url = config('services.unionbank.url') . '/convergent/v1/oauth2/token';

        $headers = [
            'content-type' => 'application/x-www-form-urlencoded',
            'accept' => 'application/json'
        ];
        
        $request = $client->post($url, [
            'headers' => $headers,
            'form_params' => [
                'client_id' => config('services.unionbank.client_id'),
                'redirect_uri' => config('services.unionbank.redirect_uri'),
                'grant_type' => 'authorization_code',
                'code' => $code
            ]
        ]);
        
        $response = json_decode($request->getBody());
        return $response;
    }

    public function getAccountInfo($userAccessToken)
    {
        $client = new \GuzzleHttp\Client();
        $url = config('services.unionbank.url') . '/accounts/v1/info';

        $request = $client->get($url, [
            'headers' => [
                'accept' => 'application/json',
                'x-ibm-client-id' => config('services.unionbank.client_id'),
                'x-ibm-client-secret' => config('services.unionbank.client_secret'),
                'Authorization' => 'Bearer ' . $userAccessToken,
            ]
        ]);

        $response = json_decode($request->getBody());
        return $response;
    }

    public function getAccountBalances($userAccessToken)
    {
        $client = new \GuzzleHttp\Client();
        $url = config('services.unionbank.url') . '/accounts/v1/balances';

        $request = $client->get($url, [
            'headers' => [
                'accept' => 'application/json',
                'x-ibm-client-id' => config('services.unionbank.client_id'),
                'x-ibm-client-secret' => config('services.unionbank.client_secret'),
                'Authorization' => 'Bearer ' . $userAccessToken,
            ]
        ]);

        $response = json_decode($request->getBody());
        return $response;
    }

    public function createPaymentURL()
    {
        $url = 'https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/authorize?client_id=' . config('services.unionbank.client_id') . '&response_type=code&scope=transfers&redirect_uri=' . config('services.unionbank.redirect_uri');
        return $url;
    }

    public function createAccountBalanceURL()
    {
        $url = 'https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/authorize?client_id=' . config('services.unionbank.client_id') . '&response_type=code&scope=account_balances&redirect_uri=' . config('services.unionbank.redirect_uri');
        return $url;
    }
}
