<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    /**
     * Crop statuses
     */
    const STATUS_AVAILABLE = 'available';
    const STATUS_OUT_OF_STOCK = 'out_of_stock';

    protected $fillable = [
        'name',
        'photo',
        'price',
        'status',
        'category_id',
        'user_id',
        'description'
    ];

    public static function getStatuses()
    {
        return [
            self::STATUS_AVAILABLE,
            self::STATUS_OUT_OF_STOCK,
        ];
    }

    public function savePhoto(UploadedFile $file)
    {
        $filename = str_random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/products/' . $this->id, $file, $filename);
        $this->photo = 'products/' . $this->id . '/' . $filename;
        $this->save();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
