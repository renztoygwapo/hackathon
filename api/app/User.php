<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'address_city',
        'address_code',
        'address_brgy',
        'address_province',
        'mobile_no',
        'user_type',
        'username'
    ];

    protected $appends = ['account_balance'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function demands()
    {
        return $this->hasMany(Demand::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getAccountBalanceAttribute()
    {
        if ($this->scope !== 'account_balances') {
            return null;
        }
        $unionbank = new UnionBank;
        return $unionbank->getAccountBalances($this->unionbank_access_token);
    }
}
