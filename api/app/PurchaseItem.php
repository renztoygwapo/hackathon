<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    protected $fillable = ['retailer_id','product_id','status'];

    public function user()
    {
        return $this->belongsTo(User::class, 'retailer_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
