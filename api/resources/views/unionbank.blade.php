<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Payment</title>
      <!-- Optional theme -->
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
   </head>
   <body>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
      <script>
         // Send a POST request
         function sendPayment () {
             axios({
             headers: {
                 'accept': 'application/json',
                 'x-ibm-client-id': "{{ config('services.unionbank.client_id') }}",
                 'x-ibm-client-secret': "{{ config('services.unionbank.client_secret') }}",
                 'Authorization': 'Bearer ' + "{{ $callbak->access_token }}" ,
                 'x-partner-id': "{{ config('services.unionbank.partner_id') }}"
             },
                         method: 'post',
                         url: 'https://api-uat.unionbankph.com/partners/sb/online/v1/transfers/single',
                         data: {
                 "senderTransferId":"00001",
                 "transferRequestDate":"2018-08-23T22:27:50Z",
                 "accountNo": "109182571452",
                 "amount": {
                 "currency":"PHP",
                 "value":"100"
                 },
                 "remarks":"Transfer remarks",
                 "particulars":"Transfer particulars",
                 "info": [
                     {
                     "index": 1,
                     "name": "Recipient",
                     "value": "Glence Comajes"
                     },
                 {
                     "index":2,
                     "name":"Message",
                     "value":"Happy Birthday"
                 }
                 ]
             }
         }).then(response => {
             alert('Payment Success!')
         }).catch(err => {
             alert('Payment Error!')
         })
         }
      </script>
      <div class="container text-center mt-4">
         <h1>Pay and purchase your order.</h1>
         <button onclick="sendPayment()" class="btn btn-warning btn-lg">PAY NOW</button>
      </div>
   </body>
</html>