<?php

use Illuminate\Database\Seeder;
use App\Category;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::firstOrCreate([
            'id' => 1,
            'name' => 'livelihood'
        ]);
        
        Category::firstOrCreate([
            'id' => 2,
            'name' => 'crops'
        ]);

        Category::firstOrCreate([
            'id' => 3,
            'name' => 'fisheries'
        ]);
    }
}
