<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address_brgy')->after('user_type');
            $table->renameColumn('address', 'address_city');
            $table->string('address_province')->after('user_type');
            $table->string('address_code')->after('user_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('address_brgy');
            $table->dropColumn('address_city');
            $table->dropColumn('address_province');
            $table->dropColumn('address_code');
        });
    }
}
