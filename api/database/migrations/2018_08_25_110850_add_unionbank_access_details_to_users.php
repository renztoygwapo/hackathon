<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnionbankAccessDetailsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('unionbank_access_token')->nullable();
            $table->text('unionbank_refresh_token')->nullable();
            $table->timestamp('unionbank_expires_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('unionbank_access_token');
            $table->dropColumn('unionbank_refresh_token');
            $table->dropColumn('unionbank_expires_at');
        });
    }
}
