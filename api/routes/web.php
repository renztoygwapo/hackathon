<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('unionbank/connect-payment', 'UnionBankController@connectPayment');
Route::get('unionbank/connect-account-balance', 'UnionBankController@connectAccountBalance');
Route::get('unionbank/callback', 'UnionBankController@callback');
Route::get('unionbank/payment', 'UnionBankController@payment');
