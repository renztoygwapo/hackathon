<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('products', 'ProductController');
Route::get('products-user', 'ProductController@productUser');
Route::apiResource('users', 'UserController');
Route::post('users-username', 'UserController@getUserByUsername');
Route::apiResource('demands', 'DemandController');
Route::apiResource('purchase-items', 'PurchaseItemController');
Route::apiResource('soil-criteria', 'SoilCriteriaController');
Route::get('products-by-category', 'ProductController@getProductsByCategoryName');
Route::get('counts', 'ProductController@countPurchasedProductsAndCropCreated');