import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import homeReducer from "../screens/Home/reducer";
import saleReducer from "../screens/Sale/reducer";
import productReducer from "../screens/Products/reducer";

export default combineReducers({
  form: formReducer,
  homeReducer,
  saleReducer,
  productReducer
});
