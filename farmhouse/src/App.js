// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import Login from "./screens/Login/";
import ForgotPassword from "./screens/ForgotPassword";
import SignUp from "./screens/SignUp/";
import Walkthrough from "./screens/Walkthrough/";
import Product from "./screens/Product";
import Story from "./screens/Story";
import Home from "./screens/Home/";
import Products from "./screens/Products";
import Sidebar from "./screens/Sidebar";
import Calendar from "./screens/Calendar/";
import Profile from "./screens/Profile/";
import Settings from "./screens/Settings";
import Sale from "./screens/Sale";

const Drawer = DrawerNavigator(
  {
    Home: { screen: Home },
    Products: { screen: Products },
    Calendar: { screen: Calendar },
    Profile: { screen: Profile },
    Settings: { screen: Settings }
  },
  {
    initialRouteName: "Home",
    contentComponent: props => <Sidebar {...props} />
  }
);

const App = StackNavigator(
  {
    Login: { screen: Login },
    SignUp: { screen: SignUp },
    ForgotPassword: { screen: ForgotPassword },
    Walkthrough: { screen: Walkthrough },
    Story: { screen: Story },
    Product: { screen: Product },
    Drawer: { screen: Drawer },
    Products: { screen: Products },
    Sale: { screen: Sale },
    Home: { screen: Home }
  },
  {
    index: 0,
    initialRouteName: "Login",
    headerMode: "none"
  }
);

export default () =>
  <Root>
    <App />
  </Root>;
