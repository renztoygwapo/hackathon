// @flow
import React, {Component} from "react";
import {Image, ImageBackground, TouchableOpacity, Platform, Linking, WebView} from "react-native";

import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View
} from "native-base";
import {Grid, Col} from "react-native-easy-grid";

import styles from "./styles";

const headerLogo = require("../../../assets/header-logo.png");
const primary = require("../../theme/variables/commonColor").brandPrimary;

class Product extends Component {
  render() {
    let params = this.props.navigation.state;
    console.log(params);
    const navigation = this.props.navigation;
    return (
      <Container style={{backgroundColor: "#fff"}}>
        <Header>
          <Left>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon active name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Image source={headerLogo} style={styles.imageHeader} />
          </Body>
          <Right />
        </Header>

        <Content showsVerticalScrollIndicator={false}>
          <View>
            <ImageBackground
              source={require("../../../assets/NewsIcons/2.jpg")}
              style={styles.newsPoster}
            >
              <View>
                <Text
                  style={
                    Platform.OS === "android"
                      ? styles.achannelHeader
                      : styles.ioschannelHeader
                  }
                >
                  { params.params.item.name }
                </Text>
                <Button
                  rounded
                  style={styles.followBtn}
                  onPress={() => this.props.navigation.navigate("Profile")}
                >
                  <Text
                    style={
                      Platform.OS === "android"
                        ? {
                            color: primary,
                            fontSize: 13,
                            fontWeight: "900",
                            textAlign: "center"
                          }
                        : {color: primary, fontSize: 13, fontWeight: "900"}
                    }
                  >
                    Buy
                  </Text>
                </Button>
                <View style={{padding: 0}}>
                  <Text style={styles.noOfFollowers}>234K Followers</Text>
                </View>
              </View>
            </ImageBackground>
          </View>

          <View foregroundColor={"white"} style={{backgroundColor: "#fff"}}>
            <TouchableOpacity
              style={{flexDirection: "row"}}
              onPress={() => navigation.navigate("Story")}
            >
              <View style={styles.newsContentWrap}>
                <Text numberOfLines={2} style={styles.newsHeader}>
                  {params.params.item.description}
                </Text>
                <Button
                  onPress={ () =>
                    this.linkToUrl()
                  }
                >
                  <Text>Proceed to Checkout</Text>
                </Button>
              </View>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
  linkToUrl(){
    url = 'https://ac8dfa0e.ngrok.io/unionbank/connect-payment'
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  }
}

export default Product;
