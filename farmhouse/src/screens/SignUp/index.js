// @flow
import React, {Component} from "react";
import {ImageBackground, StatusBar} from "react-native";
import {
  Container,
  Content,
  Text,
  Button,
  Icon,
  Item,
  Input,
  View,
  Toast,
  Left,
  Right,
  Footer,
  Picker
} from "native-base";
import {Field, reduxForm} from "redux-form";
import axios from 'axios';
import styles from "./styles";
import commonColor from "../../theme/variables/commonColor";

const required = value => (value ? undefined : "Required");
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;
const maxLength15 = maxLength(15);
const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;
const minLength8 = minLength(8);
const minLength5 = minLength(5);
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? "Only alphanumeric characters"
    : undefined;

class SignUpForm extends Component {
  constructor(props: Props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      username: '',
      city: '',
      middle_name: '',
      province: '',
      brgy: '',
      zip: '',
      user_type: '',
      mobile_no: ''
    }

    // try {
    //   const value = AsyncStorage.getItem('@OMStore:user').then((response) => {

    //     if (response === null) {
    //       this.props.navigation.navigate("LoginContainer");
    //     }
    //   }, (error) => {

    //   });
    // } catch (error) {

    // }
  }
  textInput: any;
  async signUp() {
    if (this.props.valid) {
      try {
        const data = {
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          middle_name: this.state.middle_name,
          username: this.state.username,
          mobile_no: this.state.mobile_no,
          address_city: this.state.city,
          address_province: this.state.province,
          address_brgy: this.state.brgy,
          address_code: this.state.zip,
          user_type: this.state.user_type,
        }
        const response = await axios.post('https://ac8dfa0e.ngrok.io/api/users', data);
        if(response.status === 200) {
          this.props.navigation.navigate("Products");
        }
      } catch (error) {
        console.error(error);
      }
    } else {
      Toast.show({
        text: "All the fields are compulsory!",
        duration: 2500,
        position: "top",
        textStyle: {textAlign: "center"}
      });
    }
  }

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor={commonColor.statusBarColor}
          barStyle="light-content"
        />
        <ImageBackground
          source={require("../../../assets/bg-signup.png")}
          style={styles.background}
        >
          <Content padder>
            <Text style={styles.signupHeader}>CREATE ACCOUNT</Text>
            <View style={styles.signupContainer}>
              <View>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="User Name"
                    placeholderTextColor="rgba(0,0,0,0.3)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({username: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ color: "rgba(0,0,0,0.5)", marginTop: 3 }}
                  />
                  <Input
                    placeholder="First Name"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({first_name: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="Last Name"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({last_name: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="Middle Name"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({middle_name: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="Mobile Number"
                    placeholderTextColor="rgba(255,255,255,1)"
                    keyboardType="numeric"
                    style={styles.input}
                    onChangeText={(text) => this.setState({mobile_no: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ color: "rgba(255,255,255,1)", marginTop: 3 }}
                  />
                  <Input
                    placeholder="City"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({city: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="Province"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({province: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="Barangay"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    onChangeText={(text) => this.setState({brgy: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                  <Icon
                    name="ios-person"
                    style={{ marginTop: 3 }}
                  />
                  <Input
                    placeholder="Zip Code"
                    placeholderTextColor="rgba(255,255,255,1)"
                    style={styles.input}
                    keyboardType="numeric"
                    onChangeText={(text) => this.setState({zip: text})}
                  />
              </Item>
              <Item rounded style={styles.inputGrp}>
                <Picker
                  placeholder="Select User Type"
                  selectedValue={this.state.user_type}
                  mode="dropdown"
                  style={{ height: 50, width: 100 }}
                  onValueChange={(itemValue, itemIndex) => this.setState({user_type: itemValue})}>
                  <Picker.Item label="Retailer" value="retailers" />
                  <Picker.Item label="Farmer" value="farmer" />
                </Picker>
              </Item>
              </View>

              <Button
                rounded
                bordered
                block
                onPress={() => this.signUp()}
                style={styles.signupBtn}
              >
                <Text style={{color: "#FFF"}}>Submit</Text>
              </Button>
            </View>
          </Content>
          <Footer
            style={{
              paddingLeft: 20,
              paddingRight: 20
            }}
          >
            <Left style={{flex: 2}}>
            </Left>
            <Right style={{flex: 1}}>
              <Button
                small
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Text style={styles.helpBtns}>Sign In</Text>
              </Button>
            </Right>
          </Footer>
        </ImageBackground>
      </Container>
    );
  }
}

const SignUp = reduxForm({
  form: "signup"
})(SignUpForm);
export default SignUp;
