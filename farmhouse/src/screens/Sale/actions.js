export function cameraSwitch(bool: boolean) {
  return {
    type: "CAMERA_SWITCH",
    state: bool
  };
}

export function isLoading(bool: boolean) {
  return {
    type: "IS_LOADING",
    state: bool
  };
}

export function photoCaptured(item: any) {
  return {
    type: "PHOTO",
    item
  };
}