// @flow
import React, {Component} from "react";
import {Image, ScrollView } from "react-native";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View,
  Thumbnail,
  Item,
  Input,
  Spinner,
  Picker,
  Toast
} from "native-base";
import axios from 'axios';
import {Grid, Col} from "react-native-easy-grid";
import { connect } from "react-redux";
import styles from "./styles";
import { cameraSwitch, isLoading, photoCaptured } from "./actions";
import Camera from 'react-native-camera';
const headerLogo = require("../../../assets/header-logo.png");
const primary = require("../../theme/variables/commonColor").brandPrimary;
type Props = {}
class Sale extends Component {
  constructor(props: Props) {
    super(props);
    this.state = {
      name: '',
      price: 0,
      description: '',
      loading: false,
      category_id: 0
    }
    }
    componentDidMount() {
      console.log('prop' + this.props.showCamera)
    }
  renderCamera = () => {
      if (this.props.showCamera) {
          return (
            <View style={styles.container}>
                    <Camera
                        ref={(cam) => {
                          this.camera = cam;
                          }}
                          style={styles.preview}
                          playSoundOnCapture = { true }
                          defaultTouchToFocus
                          mirrorImage={false}
                          aspect={Camera.constants.Aspect.fill}
                          captureQuality={Camera.constants.CaptureQuality.medium}
                        >
                        <Text  onPress={this.takePicture.bind(this)} style={styles.capture}>[CAPTURE]</Text>
                      </Camera>
                </View>
              );
      }
  }

  onValueChange2(value: string) {
    this.setState({
      category_id: value
    });
  }
  _defaultRender() {
    if(!this.props.showCamera) {
    return (
      <Container>
          <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon active name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Image source={headerLogo} style={styles.imageHeader} />
          </Body>
          <Right />
        </Header>
          <View style={styles.overviewHeaderContainer}>
          <Text style={styles.overviewHeader}>Start Sale Now</Text>
          <Text note style={styles.overviewHead}>
            Take a Picture to your backyard
          </Text>
          </View>
          <Content
            showsVerticalScrollIndicator={false}
            style={{backgroundColor: "#fff"}}
          >
          <View style={{alignSelf: "center", marginTop: 50}}>
          <Button large
          onPress = {() => { this.props.switchCamera(true) }}
          >
            <Icon name='camera' />
            <Text> Take a Picture </Text>
          </Button>
        </View>

        <View style={styles.signupContainer}>
          <View>
          <Item rounded style={styles.inputGrp}>
              <Icon
                name="ios-person"
                style={{ color: "rgba(0,0,0,0.5)", marginTop: 3 }}
              />
              <Input
                placeholder="Name"
                placeholderTextColor="rgba(0,0,0,0.3)"
                style={styles.input}
                onChangeText={(text) => this.setState({name: text})}
              />
          </Item>
          </View>
          <View>
          <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="ios-arrow-down-outline" />}
                style={{ width: undefined }}
                placeholder="Select Category"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.category_id}
                onValueChange={this.onValueChange2.bind(this)}
              >
                <Picker.Item label="Crops" value="1" />
                <Picker.Item label="Livelihood" value="2" />
                <Picker.Item label="Fisheries" value="3" />
              </Picker>
            </Item>
          </View>
          <View>
          <Item rounded style={styles.inputGrp}>
              <Icon
                name="ios-pricetags"
                style={{ color: "rgba(0,0,0,0.5)", marginTop: 3 }}
              />
              <Input
                placeholder="Price"
                keyboardType="numeric"
                placeholderTextColor="rgba(0,0,0,0.3)"
                style={styles.input}
                onChangeText={(text) => this.setState({price: text})}
              />
          </Item>
          </View>
          <View>
          <Item regular style={{ marginTop: 10 }}>
              <Input
                placeholder="Product Description..."
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.inputBox}
                multiline
                returnKeyType="default"
                onChangeText={(text) => this.setState({description: text})}
              />
            </Item>
          </View>
            <View>
            <Button
              large
              rounded
              style={{ alignSelf: "flex-end", marginTop: 10, height: 40 }}
              onPress={() => this._addSale()}
            >
              { this._buttonText()}
            </Button>
            </View>
        </View>
          </Content>
      </Container>
    )
  }
  }

  _buttonText() {
    if(this.state.loading){
      return (
        <Spinner color="white"/>
      )
    } else {
      return (
        <Text>Submit</Text>
      )
    }
  }

  async _addSale () {
    this.setState({
      loading: true
    })
    try {
      const image_data = new FormData()
      image_data.append('photo', {
        uri: this.props.photo.mediaUri,
        type: 'image/jpg',
        name: 'testPhotoName.jpg'
      })
      image_data.append('name', this.state.name)
      image_data.append('description', this.state.description)
      image_data.append('price', this.state.price)
      image_data.append('status', 'available')
      image_data.append('category_id', this.state.category_id)
      const response = await axios.post('https://ac8dfa0e.ngrok.io/api/products', image_data, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        }
      });
      if(response.status === 201) {
        this.props.navigation.goBack();
        Toast.show({
          text: "Products Added Successfully!",
          buttonText: "Okay",
          position: "top",
          type: "success",
          duration: 3000
        })
      }
    } catch (error) {
      console.log(error);
    }
  }

  takePicture() {
    const options = {};
    //options.location = ...
    this.camera.capture({metadata: options})
    .then(data => {
      this.props.photoCaptured(data)
      this.camera.stopPreview()
      this.props.switchCamera(false)
      console.log(data)
    })
    .catch(err => console.error(err));
    }

  render() {
    const navigation = this.props.navigation;
    return (
      <Container style={{backgroundColor: "#fff"}}>
      {this._defaultRender()}
      {this.renderCamera()}
      </Container>
    );
  }
}


function bindAction(dispatch) {
  return {
    switchCamera: url => dispatch(cameraSwitch(url)),
    isLoading: url => dispatch(isLoading(url)),
    photoCaptured: url => dispatch(photoCaptured(url))
  };
}

const mapStateToProps = state => ({
  showCamera: state.saleReducer.showCamera,
  loading: state.saleReducer.loading,
  photo: state.saleReducer.photo
});
export default connect(mapStateToProps, bindAction)(Sale);
