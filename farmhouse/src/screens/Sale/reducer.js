const initialState = {
  showCamera: false,
  loading: false,
  photo: {}
};
export default function(state: any = initialState, action: Function) {
  switch (action.type) {
    case "CAMERA_SWITCH":
      return { ...state, showCamera: action.state }; 
    case "IS_LOADING":
      return { ...state, loading: action.state };
    case "PHOTO":
      return { ...state, photo: action.item };   
    default:
      return state;
  }
}
