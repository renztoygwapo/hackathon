const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const primary = require("../../theme/variables/commonColor").brandPrimary;
export default {
  container: {
    flex: 1,
    width: null,
    height: null
  },
  newsPoster: {
    height: deviceHeight / 4 + 20,
    width: null,
    position: "relative"
  },
  ioschannelHeader: {
    fontSize: 20,
    fontWeight: "900",
    alignSelf: "center",
    padding: 20,
    paddingTop: 30
  },
  achannelHeader: {
    fontSize: 20,
    fontWeight: "900",
    alignSelf: "center",
    padding: 20,
    marginTop: 10,
    textAlign: "center"
  },
  followBtn: {
    alignSelf: "center",
    backgroundColor: "#fff"
  },
  noOfFollowers: {
    fontSize: 12,
    fontWeight: "900",
    alignSelf: "center",
    textAlign: "center",
    padding: 20,
    paddingTop: 10,
    marginTop: 10
  },
  newsContentWrap: {
    flexDirection: "column",
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: "#ddd"
  },
  newsHeader: {
    color: "#444",
    fontWeight: "bold"
  },
  newsContent: {
    paddingTop: 20,
    paddingBottom: 20
  },
  newsLink: {
    color: Platform.OS === "android" ? "#777" : "#666",
    fontSize: 12,
    alignSelf: "flex-start",
    fontWeight: "bold"
  },
  timeIcon: {
    fontSize: 20,
    paddingRight: 10,
    color: "#666",
    marginLeft: Platform.OS === "android" ? 15 : 0,
    paddingLeft: Platform.OS === "android" ? 15 : 20,
    marginTop: Platform.OS === "android" ? -1 : -3
  },
  imageHeader: {
    height: 25,
    width: 95,
    resizeMode: "contain"
  },
  overviewHeaderContainer: {
    padding: 20,
    paddingTop: 30,
    alignItems: "center",
    backgroundColor: primary
  },
  overviewHeader: {
    fontSize: 22,
    lineHeight: 30,
    fontWeight: "900",
    alignSelf: "center"
  },
  overviewHead: {
    opacity: 0.9,
    fontSize: 14,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#FFF"
  },
  overviewContent: {
    padding: 20,
    backgroundColor: "#FFF"
  },
  overviewTopicsBox: {
    paddingBottom: 20
  },
  overviewInfoHeader: {
    alignSelf: "flex-start",
    fontSize: 14,
    fontWeight: "900",
    color: "#000"
  },
  overviewInfoPerc: {
    alignSelf: "flex-end",
    fontSize: 14,
    fontWeight: "900",
    color: "#000"
  },
  container: {
  flex: 1,
  flexDirection: 'row',
  },
  preview: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center'
  },
  capture: {
  flex: 0,
  backgroundColor: '#fff',
  borderRadius: 5,
  color: '#000',
  padding: 10,
  margin: 40
  },
  inputGrp: {
    flexDirection: "row",
    borderRadius: 25,
    backgroundColor: "rgba(255,255,255,0.2)",
    marginBottom: 10,
    borderWidth: 0,
    borderColor: "transparent"
  },
  inputBox: {
    maxHeight: 300,
    minHeight: 200,
    textAlignVertical: "top"
  },
  inputBoxIcon: {
    alignSelf: "flex-end"
  },
  signupContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop:
      deviceWidth < 330
        ? Platform.OS === "android"
          ? deviceHeight / 9 - 20
          : deviceHeight / 10 - 20
        : Platform.OS === "android"
          ? deviceHeight / 9 - 20
          : deviceHeight / 8 - 20
  },
};
