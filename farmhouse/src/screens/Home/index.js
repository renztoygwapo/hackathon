// @flow
import React, {Component} from "react";
import {
  Platform,
  Image,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  FlatList,
  View as RNView
} from "react-native";
import {connect} from "react-redux";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View,
  Spinner,
  Card,
  CardItem
} from "native-base";
import { cropsItem } from "./actions";
import axios from 'axios';
import {Grid, Col, Row} from "react-native-easy-grid";
import Carousel from "react-native-carousel-view";

import {itemsFetchData} from "../../actions";
import datas from "./data.json";

import styles from "./styles";

const deviceWidth = Dimensions.get("window").width;
const headerLogo = require("../../../assets/header-logo.png");

class Home extends Component {
  componentDidMount() {
    this.getProductCrops()
  }
  async getProductCrops() {
    try {
      const response = await axios.get('https://ac8dfa0e.ngrok.io/api/products?category_name=crops');
      if(response.data.data.length > 0) {
        this.props.cropsItem(response.data.data)
      }
    } catch (error) {
      console.error(error);
    }
  }
  addProduct() {
    this.props.navigation.navigate("Product")
  }
  _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={{flexDirection: "row"}}
        onPress={() => this.props.navigation.navigate("Story")}
      >
        <View style={styles.newsContent}>
          <Text numberOfLines={2} style={styles.newsHeader}>
            {item.headline}
          </Text>
          <Grid style={styles.swiperContentBox}>
            <Col style={{flexDirection: "row"}}>
              <Text style={styles.newsLink}>
                {item.link}
              </Text>
              <Icon name="ios-time-outline" style={styles.timeIcon} />
              <Text style={styles.newsLink}>
                {item.time}
              </Text>
            </Col>
            <Col>
              <TouchableOpacity
                style={styles.newsTypeView}
                onPress={() => this.props.navigation.navigate("Channel")}
              >
                <Text style={styles.newsTypeText}>
                  {item.category}
                </Text>
              </TouchableOpacity>
            </Col>
          </Grid>
        </View>
      </TouchableOpacity>
    );
  };
  render() {
      return (
        <Container>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              >
                <Icon active name="menu" />
              </Button>
            </Left>
            <Body>
              <Image source={headerLogo} style={styles.imageHeader} />
            </Body>
            <Right />
          </Header>
          <Content
            showsVerticalScrollIndicator={false}
            style={{backgroundColor: "#fff"}}
          >
            <View>
              <View text="test">
                {/* FarmHouse */}
                {/* <Card style={{backgroundColor: '#000'}}> */}
                {/* <CardItem header> */}
                {/* Turn your home into a Farm House */}
                <Text style={styles.titleText}>Let's Make your Land a Money Maker Backyard</Text>
                {/* </CardItem> */}
                {/* </Card> */}
                {/* <Carousel
                  width={deviceWidth}
                  height={330}
                  indicatorAtBottom
                  indicatorSize={Platform.OS === "android" ? 15 : 10}
                  indicatorColor="#FFF"
                  indicatorOffset={10}
                  animate={false}
                >
                  <RNView>
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => this.props.navigation.navigate("Story")}
                      style={styles.slide}
                    >
                      <ImageBackground
                        style={styles.newsPoster}
                        source={require("../../../assets/NewsIcons/1.jpg")}
                      >
                        
                      </ImageBackground>
                    </TouchableOpacity>
                  </RNView>
                  <RNView>
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => this.props.navigation.navigate("Story")}
                      style={styles.slide}
                    >
                      <ImageBackground
                        style={styles.newsPoster}
                        source={require("../../../assets/NewsIcons/3.jpg")}
                      >
                        <View style={styles.swiperTextContent}>
                          <Text
                            numberOfLines={2}
                            style={styles.newsPosterHeader}
                          >
                            So that the applications are able to load faster and
                            resize easily.
                          </Text>
                          <Grid style={styles.swiperContentBox}>
                            <Col style={{flexDirection: "row"}}>
                              <Text style={styles.newsPosterLink}>CDC</Text>
                              <Icon
                                name="ios-time-outline"
                                style={styles.timePosterIcon}
                              />
                              <Text style={styles.newsPosterLink}>2hr ago</Text>
                            </Col>
                            <Col>
                              <TouchableOpacity
                                style={styles.newsPosterTypeView}
                              >
                                <Text
                                  style={styles.newsPosterTypeText}
                                  onPress={() =>
                                    this.props.navigation.navigate("Channel")}
                                >
                                  ENVIRONMENT
                                </Text>
                              </TouchableOpacity>
                            </Col>
                          </Grid>
                        </View>
                      </ImageBackground>
                    </TouchableOpacity>
                  </RNView>
                  <RNView>
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => this.props.navigation.navigate("Story")}
                      style={styles.slide}
                    >
                      <ImageBackground
                        style={styles.newsPoster}
                        source={require("../../../assets/NewsIcons/4.jpg")}
                      >
                        <View style={styles.swiperTextContent}>
                          <Text
                            numberOfLines={2}
                            style={styles.newsPosterHeader}
                          >
                            But still look sharp on high-definition screens.
                          </Text>
                          <Grid style={styles.swiperContentBox}>
                            <Col style={{flexDirection: "row"}}>
                              <Text style={styles.newsPosterLink}>SKY.com</Text>
                              <Icon
                                name="ios-time-outline"
                                style={styles.timePosterIcon}
                              />
                              <Text style={styles.newsPosterLink}>
                                1day ago
                              </Text>
                            </Col>
                            <Col>
                              <TouchableOpacity
                                style={styles.newsPosterTypeView}
                              >
                                <Text
                                  style={styles.newsPosterTypeText}
                                  onPress={() =>
                                    this.props.navigation.navigate("Channel")}
                                >
                                  WORLD
                                </Text>
                              </TouchableOpacity>
                            </Col>
                          </Grid>
                        </View>
                      </ImageBackground>
                    </TouchableOpacity>
                  </RNView>
                </Carousel> */}
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                height: 100,
                padding: 20,
                marginRight: 10,
              }}>
              <Button
                style={styles.exploreBtn}
                full
                onPress={ () => this.props.navigation.navigate("Sale")}
                title="Learn More"
                accessibilityLabel="Learn more about this purple button"
              >
                <Text style={styles.explore}>Start Sell Now!</Text>
              </Button>        
            </View>

            {/* <TouchableOpacity onPress={ () => this.props.navigation.navigate("Product")}> */}
            {/* <Card style={{backgroundColor: '#000'}}>
                <CardItem header>
                  <Text style={{color: '#000'}}>Are you ready to Start Selling ?</Text>
                </CardItem>
                <CardItem footer>
                  <Text style={{color: '#000'}}>See More</Text>
                </CardItem>
            </Card> */}
            


            {/* <Card>
              <CardItem header>
                <Text style={{color: '#000'}}>Do you want to know what type of Crops or Livelihood you had in Backyard?</Text>
              </CardItem>
              <CardItem footer>
                <Text style={{color: '#000'}}>See More</Text>
              </CardItem>
            </Card> */}
          </Content>
        </Container>
      );
  }
}

function bindAction(dispatch) {
  return {
    fetchData: url => dispatch(itemsFetchData(url)),
    cropsItem: url => dispatch(cropsItem(url))
  };
}

const mapStateToProps = state => ({
  items: state.homeReducer.items,
  hasErrored: state.homeReducer.hasErrored,
  isLoading: state.homeReducer.isLoading,
  crops: state.homeReducer.crops
});
export default connect(mapStateToProps, bindAction)(Home);
