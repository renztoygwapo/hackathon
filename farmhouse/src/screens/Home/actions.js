export function itemsHasErrored(bool: boolean) {
  return {
    type: "ITEMS_HAS_ERRORED",
    hasErrored: bool
  };
}
export function itemsIsLoading(bool: boolean) {
  return {
    type: "ITEMS_IS_LOADING",
    isLoading: bool
  };
}
export function itemsFetchDataSuccess(items: Object) {
  return {
    type: "ITEMS_FETCH_DATA_SUCCESS",
    items
  };
}

export function cropsItem(crops: any) {
  return {
    type: "CROPS",
    crops
  };
}

export function livelihoodItem(livelihood: any) {
  return {
    type: "LIVELIHOOD",
    livelihood
  };
}

export function fisheriesItem(fisheries: any) {
  return {
    type: "FISHERIES",
    fisheries
  };
}