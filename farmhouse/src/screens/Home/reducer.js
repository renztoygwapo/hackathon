const initialState = {
  isLoading: true,
  hasErrored: false,
  items: [],
  crops: [],
  livelihood: [],
  fisheries: []
};
export default function(state: any = initialState, action: Function) {
  switch (action.type) {
    case "ITEMS_HAS_ERRORED":
      return { ...state, hasErrored: action.hasErrored };
    case "ITEMS_IS_LOADING":
      return { ...state, isLoading: action.isLoading };
    case "ITEMS_FETCH_DATA_SUCCESS":
      return { ...state, items: action.items };
    case "CROPS":
      return { ...state, crop_items: action.crops }; 
    case "LIVELIHOOD":
      return { ...state, crop_items: action.livelihood }; 
    case "FISHERIES":
      return { ...state, crop_items: action.fisheries }; 
    default:
      return state;
  }
}
