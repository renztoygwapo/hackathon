// @flow
import React, { Component } from "react";
import { Image,FlatList,TouchableOpacity, Platform } from "react-native";
import axios from 'axios';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Tabs,
  Tab,
  Text,
  TabHeading,
  Spinner,
  View,
  Card,
  CardItem,
  Thumbnail,
  Badge
} from "native-base";

import { cropsItem,livelihoodItem } from "./actions.js";
import {connect} from 'react-redux';
import styles from "./styles";
import TabOne from "./tabOne";
import TabTwo from "./tabTwo";
import TabThree from "./tabThree";
const primary = require("../../theme/variables/commonColor").brandPrimary;
const headerLogo = require("../../../assets/header-logo.png");
type Props = {}
class Products extends Component {
  constructor(props: Props) {
    super(props);
    this.state = {
    }
  }
  componentDidMount() {
    this.getProductCrops()
    console.log(this.props.cropsItem)
  }

  async getProductCrops() {
    try {
      const response = await axios.get('https://ac8dfa0e.ngrok.io/api/products?category_name=crops');
      if(response.data.data.length > 0) {
        this.props.cropsItem(response.data.data)
      }
    } catch (error) {
      console.error(error);
    }
  }

  async getProductLivelihood() {
    try {
      const response = await axios.get('https://ac8dfa0e.ngrok.io/api/products?category_name=livelihood');
      if(response.data.data.length > 0) {
        this.props.cropsItem(response.data.data)
      }
    } catch (error) {
      console.error(error);
    }
  }

  _renderCrops(){
    if(this.props.crops.length>0) {
        return (
          <TabOne navigation={this.props.navigation} />
        )
      } else {
        return (
          <View>
            <Spinner />
          </View>
        )
      }
  }

  _renderItem = ({ item }) => {
    return (
      <Card style={{backgroundColor: primary}}>
          <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Product", {
                    item: item
                  })}
                >
            <CardItem >
            <Image
              style={{
                alignSelf: 'center',
                height: 150,
                width: 150
              }}
              source={{uri: 'https://ac8dfa0e.ngrok.io/' + item.photo}}
              resizeMode="stretch"
              />
                <Body>
                  <Text style={{color: '#000', padding: 15, fontSize: 20}}>{item.name}</Text>
                  <Text style={{color: primary, padding: 15, fontSize: 20}}>{item.price}</Text>
                  <Badge success style={{marginLeft: 10}}>
                    <Text>{item.status}</Text>
                  </Badge>
                </Body>
            </CardItem>
          </TouchableOpacity>
      </Card>
    );
  }

  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon active name="menu" />
            </Button>
          </Left>
          <Body>
            <Image source={headerLogo} style={styles.imageHeader} />
          </Body>
          <Right />
        </Header>
        <Tabs style={{ backgroundColor: "#fff" }}>
          <Tab
            heading={
              <TabHeading>
                <Text>All</Text>
              </TabHeading>
            }
          >
            <View>
            <FlatList
              data={this.props.crops}
              renderItem={this._renderItem}
              keyExtractor={item => item.id}
            />
            </View>
            <View>
              {this._renderCrops()}
            </View>
          </Tab>
          <Tab heading="Crops">
            <TabTwo navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    cropsItem: url => dispatch(cropsItem(url)),
    livelihoodItem: url => dispatch(livelihoodItem(url)),
  };
}

const mapStateToProps = state => ({
  crops: state.productReducer.crops,
  livelihood: state.productReducer.livelihood,
});
export default connect(mapStateToProps, bindAction)(Products);
