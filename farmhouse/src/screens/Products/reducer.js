const initialState = {
    crops: [],
    livelihood: [],
    fisheries: []
  };
  export default function(state: any = initialState, action: Function) {
    switch (action.type) {
      case "CROPS":
        return { ...state, crops: action.crops }; 
      case "LIVELIHOOD":
        return { ...state, livelihood: action.livelihood }; 
      case "FISHERIES":
        return { ...state, fisheries: action.fisheries }; 
      default:
        return state;
    }
  }
  