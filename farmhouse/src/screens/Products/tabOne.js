// @flow
import React, {Component} from "react";
import {ImageBackground, TouchableOpacity, Platform} from "react-native";
import {Content, Text, View} from "native-base";
import {Grid, Col, Row} from "react-native-easy-grid";
import styles from "./styles";
import {connect} from 'react-redux';

class TabOne extends Component {
  constructor(props: Props) {
    super(props);
    this.state = {
    }
  }

  _renderCrops() {
    <Grid>
    {this.props.crops.map((prop, key) => {
      return (
          <Row>
            <Text style={{ color: "#000" }}>
              {prop.name}
            </Text>
          </Row>
      );
    })}
  </Grid>
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <Content showsVerticalScrollIndicator={false}>
        <View>
            {this._renderCrops()}
        </View>
      </Content>
    );
  }
}

function bindAction(dispatch) {
  return {
    cropsItem: url => dispatch(cropsItem(url)),
  };
}

const mapStateToProps = state => ({
  crops: state.productReducer.crops,
});
export default connect(mapStateToProps, bindAction)(TabOne);