export function cropsItem(crops: any) {
    return {
      type: "CROPS",
      crops
    };
  }

  export function livelihoodItem(livelihood: any) {
    return {
      type: "LIVELIHOOD",
      livelihood
    };
  }

  export function fisheriesItem(fisheries: any) {
    return {
      type: "FISHERIES",
      fisheries
    };
  }